//5.ตรวจสอบว่าใน Array มีเลขคู่หรือไม่
var numbers = [5, 14, 9, 16, 10];
// Every Method
//var everyResult = numbers.every(myFunction);
//console.log(everyResult); // false
// Some Method
var someResult = numbers.some(myFunction);
function myFunction(value, index) {
    return value%2 == 0;
}
console.log(someResult); // true
